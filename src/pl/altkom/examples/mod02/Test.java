package pl.altkom.examples.mod02;

public class Test {

	public static void main(String[] args) {
		Date start = new Date(1990, 10, 12);
		Date end = new Date(2005, 10, 24);
		System.out.println("Start: " + start + "\n" + "End: " + end);
		
		Date today = start;
		
		System.out.println("Today: " + today);
		today.year = 1999;
		today.day = 24;
		
		System.out.println("Today: " + today + "\n" + "Start: " + start);
		end = today;
		System.out.println("End: " + end);
	}
}
