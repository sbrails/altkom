package pl.altkom.examples.mod02;

public class Trip {
	Date start, end;
	String destination;
	
	public Trip(Date start, Date end, String destination) {
		this.start = start;
		this.end = end;
		this.destination = destination;
	}
	
	public String getInfo() {
		return "Wylot do "+ destination + ", dnia: " + start.getInfo()
				+ ", powrot: " + end.getInfo();
	}
}
