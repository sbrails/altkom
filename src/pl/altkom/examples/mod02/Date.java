package pl.altkom.examples.mod02;

public class Date {
	int day, month, year;
	
	public Date(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	public String getInfo(){
		return day + "-" + month + "-" + year;
	}
	
}
