package pl.altkom.examples.mod02;

public class TravelOffice {

	public static void main(String[] args) {
		Customer customer = new Customer("Janek Kowalski");
		Trip trip = new Trip(new Date(2014,04,20), new Date(2014,04,27), "Pcim Dolny");
		customer.assignTrip(trip);
		System.out.println(customer.getInfo());
	}

}
